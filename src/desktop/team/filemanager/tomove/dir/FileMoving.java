package desktop.team.filemanager.tomove.dir;

import desktop.team.filemanager.filesearcher.dir.PathManager;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashSet;

public class FileMoving {


    /**
     * @param paths - Объект класса PathManager, который содержит в себе путь таргета и путь назначения
     *              <p>
     *              Метод для перемещения всей папки файлов.
     */
    static public void dirMoving(PathManager paths) {
        if (Files.exists(paths.getTargetPath(), LinkOption.NOFOLLOW_LINKS) && Files.isDirectory(paths.getTargetPath(), LinkOption.NOFOLLOW_LINKS)) {
            try (DirectoryStream<Path> stream = Files.newDirectoryStream(paths.getTargetPath())) {
                for (Path entry : stream) {
                    if (Files.exists(paths.getPathDestination(), LinkOption.NOFOLLOW_LINKS) && Files.isDirectory(paths.getPathDestination(), LinkOption.NOFOLLOW_LINKS)) {
                        Files.move(entry, Paths.get(paths.getPathDestination() + "\\" + entry.getFileName()));
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
    }

    /**
     * @param paths - Объект класса PathManager, который содержит в себе путь таргета и путь назначения
     *              <p>
     *              Метод для перемещения одного файла.
     */
    static public void oneFileMoving(PathManager paths) {
        if (Files.exists(paths.getTargetPath(), LinkOption.NOFOLLOW_LINKS) && !Files.isDirectory(paths.getTargetPath(), LinkOption.NOFOLLOW_LINKS)) {
            try {
                if (Files.exists(paths.getPathDestination(), LinkOption.NOFOLLOW_LINKS) && Files.isDirectory(paths.getPathDestination(), LinkOption.NOFOLLOW_LINKS)) {
                    Files.copy(paths.getTargetPath(), Paths.get(paths.getPathDestination() + "//" + paths.getTargetPath().getFileName()), StandardCopyOption.REPLACE_EXISTING);
                    Files.delete(paths.getTargetPath());
                }
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
    }

    static public void getFileTree(PathManager paths) {
        try {
            Files.walkFileTree(paths.getTargetPath(), new HashSet<>(), 2, new FileVisitor<Path>()
            {
                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                        throws IOException {
                    System.out.println("preVisitDirectory: " + dir);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
                        throws IOException {
                    System.out.println("visitFile: " + file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFileFailed(Path file, IOException exc)
                        throws IOException {
                    System.out.println("visitFileFailed: " + file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc)
                        throws IOException {
                    System.out.println("postVisitDirectory: " + dir);
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}



