package desktop.team.filemanager.todelete.dir;

import desktop.team.filemanager.filesearcher.dir.PathManager;

import java.io.IOException;
import java.nio.file.*;

public class FileDelete {

    /**
     * @param paths - Объект класса PathManager, который содержит в себе путь таргета и путь назначения
     *
     *              Метод для удаления одного файла.
     */
    static public void oneFileDelete(PathManager paths){
        if (Files.exists(paths.getTargetPath(), LinkOption.NOFOLLOW_LINKS) && !Files.isDirectory(paths.getTargetPath(), LinkOption.NOFOLLOW_LINKS))
        {
            try
            {
                    Files.delete(paths.getTargetPath());
            }
            catch (IOException e)
            {
                e.printStackTrace();
                System.exit(1);
            }
        }
    }


    /**
     * @param paths - Объект класса PathManager, который содержит в себе путь таргета и путь назначения
     *
     *              Метод для удаления директории и её содержимого.
     */
    static public void dirDelete(PathManager paths){
        if (Files.exists(paths.getTargetPath(), LinkOption.NOFOLLOW_LINKS) && Files.isDirectory(paths.getTargetPath(), LinkOption.NOFOLLOW_LINKS))
        {
            try (DirectoryStream<Path> stream = Files.newDirectoryStream(paths.getTargetPath()))
            {
                for (Path entry : stream)
                {
                        Files.delete(entry);
                }
                Files.delete(paths.getTargetPath());
            }
            catch (IOException e)
            {
                e.printStackTrace();
                System.exit(1);
            }
        }
    }

}
