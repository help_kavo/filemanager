package desktop.team.filemanager.filesearcher.dir;

import java.nio.file.Path;
import java.nio.file.Paths;

public class PathManager {

    private String sPathTarget; //Путь изначального ресурса
    private String sPathDestination; //Путь назначения
    private Path pathTarget; //Путь тип Path изначального ресурса
    private Path pathDestination; //Путь тип Path назначения

    public PathManager(String sPathTarget, String sPathDestination) {
        this.sPathTarget = sPathTarget;
        this.sPathDestination = sPathDestination;
        pathTarget = Paths.get(sPathTarget);
        pathDestination = Paths.get(sPathDestination);
    }

    public Path getTargetPath(){
        return pathTarget;
    }

    public Path getPathDestination(){
        return pathDestination;
    }

    public void setTargetPath(String sPathTarget){
        this.sPathTarget = sPathTarget;
        pathTarget = Paths.get(sPathTarget);
    }

    public void setPathDestination(String sPathDestination){
        this.sPathDestination = sPathDestination;
        pathDestination = Paths.get(sPathDestination);
    }

}
